
export const toggleLoader = (value) => {
  return {
    type: 'TOGGLE_LOADER',
    value
  }
}