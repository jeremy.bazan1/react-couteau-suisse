const INITIAL_STATE = {
  counter: 0,
  randomNumber: null,
  fetchedHistory: []
};

//Implement the reducer
function counterReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        counter: state.counter + 1
      };
    case 'DECREMENT':
      return {
        ...state,
        counter: state.counter - 1
      };
    case 'ADD':
      return {
        ...state,
        counter: state.counter + action.value
      };
    case 'FETCH_DATA':
      console.log(state.fetchedHistory);
      
      return {
        ...state,
        randomNumber: action.value,
        fetchedHistory: [
          ...state.fetchedHistory,
          action.value
        ]

      }
    default:
      return state;
  }
}

export default counterReducer;