const INITIAL_STATE = {
  toggle: false
};

//Implement the reducer
function toggleReducer (state = INITIAL_STATE, action){
  switch (action.type) {
    case 'TOGGLE_MENU':
      return {
        toggle: !state.toggle
      };
    default:
      return state;
  }
}

export default toggleReducer;