import React from 'react';

class Loader extends React.Component {

  render() {
    return (
      <div>
        <img src="https://i.redd.it/o6m7b0l6h6pz.gif" width="100px"/>
      </div>
    );
  }
}

export default Loader;